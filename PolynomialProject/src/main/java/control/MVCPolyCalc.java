package control;

import view.PolyView;
import model.Polynomial;

public class MVCPolyCalc {

	public static void main(String[] args) {
		PolyView theView = new PolyView();
		// set initial values
		//theView.setFxPoly("5x^3+2x^1+50.2x^0");
		//theView.setGxPoly("2x^2+2x^0");
		Polynomial thePoly = new Polynomial();
		PolyControl theController = new PolyControl(theView, thePoly);
		theView.setVisible(true);
	}
}
