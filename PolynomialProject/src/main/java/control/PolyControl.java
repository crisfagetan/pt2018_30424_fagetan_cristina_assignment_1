package control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import view.PolyView;
import model.Polynomial;

public class PolyControl {

	private PolyView theView;
	private Polynomial thePoly;

	public PolyControl(PolyView theView, Polynomial thePoly) {
		this.theView = theView;
		this.thePoly = thePoly;

		this.theView.addListener(new AddListener());
		this.theView.sub1Listener(new Sub1Listener());
		this.theView.sub2Listener(new Sub2Listener());
		this.theView.mulListener(new MulListener());
		this.theView.div1Listener(new Div1Listener());
		this.theView.div2Listener(new Div2Listener());
		this.theView.derivfListener(new DerivfListener());
		this.theView.derivgListener(new DerivgListener());
		this.theView.integfListener(new IntegfListener());
		this.theView.integgListener(new InteggListener());

	}

	class AddListener implements ActionListener {

		public void actionPerformed(ActionEvent add) {
			try {
				Polynomial fx = Polynomial.fromString(theView.getFxPoly());
				Polynomial gx = Polynomial.fromString(theView.getGxPoly());
				Polynomial hx = fx.addition(gx);
				hx.reduce();
				theView.setHxPoly(hx.toString());
			} catch (NumberFormatException e) {
				System.out.println("Wrong input");
			}
		}

	}

	class Sub1Listener implements ActionListener {

		public void actionPerformed(ActionEvent sub1) {
			try {
				Polynomial fx = Polynomial.fromString(theView.getFxPoly());
				Polynomial gx = Polynomial.fromString(theView.getGxPoly());
				fx = fx.substraction(gx);
				fx.reduce();
				theView.setHxPoly(fx.toString());
			} catch (NumberFormatException e) {
				System.out.println("Wrong input");
			}
		}
	}

	class Sub2Listener implements ActionListener {

		public void actionPerformed(ActionEvent sub2) {
			try {
				Polynomial fx = Polynomial.fromString(theView.getFxPoly());
				Polynomial gx = Polynomial.fromString(theView.getGxPoly());
				gx = gx.substraction(fx);
				gx.reduce();
				theView.setHxPoly(gx.toString());
			} catch (NumberFormatException e) {
				System.out.println("Wrong input");
			}
		}
	}

	class DerivfListener implements ActionListener {

		public void actionPerformed(ActionEvent derivf) {
			try {
				Polynomial fx = Polynomial.fromString(theView.getFxPoly());
				fx = fx.derive();
				fx.reduce();
				theView.setHxPoly(fx.toString());
			} catch (NumberFormatException e) {
				System.out.println("Wrong input");
			}
		}
	}

	class DerivgListener implements ActionListener {

		public void actionPerformed(ActionEvent derivg) {
			try {
				Polynomial gx = Polynomial.fromString(theView.getGxPoly());
				gx = gx.derive();
				gx.reduce();
				theView.setHxPoly(gx.toString());
			} catch (NumberFormatException e) {
				System.out.println("Wrong input");
			}
		}
	}

	class IntegfListener implements ActionListener {

		public void actionPerformed(ActionEvent integf) {
			try {
				Polynomial fx = Polynomial.fromString(theView.getFxPoly());
				fx = fx.integrate();
				fx.reduce();
				theView.setHxPoly(fx.toString());
			} catch (NumberFormatException e) {
				System.out.println("Wrong input");
			}
		}
	}

	class InteggListener implements ActionListener {

		public void actionPerformed(ActionEvent derivg) {
			try {
				Polynomial gx = Polynomial.fromString(theView.getGxPoly());
				gx = gx.integrate();
				gx.reduce();
				theView.setHxPoly(gx.toString());
			} catch (NumberFormatException e) {
				System.out.println("Wrong input");
			}
		}
	}

	class MulListener implements ActionListener {

		public void actionPerformed(ActionEvent mul) {
			try {
				Polynomial fx = Polynomial.fromString(theView.getFxPoly());
				Polynomial gx = Polynomial.fromString(theView.getGxPoly());
				fx = fx.multiply(gx);
				fx.reduce();
				theView.setHxPoly(fx.toString());
			} catch (NumberFormatException e) {
				System.out.println("Wrong input");
			}
		}

	}

	class Div1Listener implements ActionListener {

		public void actionPerformed(ActionEvent div1) {
			try {
				Polynomial fx = Polynomial.fromString(theView.getFxPoly());
				Polynomial gx = Polynomial.fromString(theView.getGxPoly());
				Polynomial hx = fx.divide(gx);
				theView.setHxPoly(hx.toString());
			} catch (NumberFormatException e) {
				System.out.println("Wrong input");
			}
		}

	}

	class Div2Listener implements ActionListener {

		public void actionPerformed(ActionEvent div2) {
			try {
				Polynomial fx = Polynomial.fromString(theView.getFxPoly());
				Polynomial gx = Polynomial.fromString(theView.getGxPoly());
				gx = gx.divide(fx);
				theView.setHxPoly(gx.toString());
			} catch (NumberFormatException e) {
				System.out.println("Wrong input");
			}
		}

	}
}
