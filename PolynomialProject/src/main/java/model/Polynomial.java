package model;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Polynomial{

	private ArrayList<Monomial> listaMonoame = new ArrayList<Monomial>();

	// reduces and put the polynomial in the right order
	public void reduce() {
		Polynomial reduced = new Polynomial();
		for (Monomial m : this.listaMonoame) {
			boolean found = false;
			for (Monomial m2 : reduced.listaMonoame) {
				if (m.getExp() == m2.getExp()) {
					found = true;
					m2.add(m);
				}
			}
			if (!found) {
				reduced.insertMonomial(m);
			}
		}
		Collections.sort(reduced.listaMonoame);
		this.listaMonoame = reduced.listaMonoame;
	}

	public void insertMonomial(Monomial m) {
		this.listaMonoame.add(m);
	}

	public void removeMonomial(Monomial m) {
		if (m.coeff == 0)
			this.listaMonoame.remove(m);

	}

	public Polynomial addition(Polynomial p) {

		Polynomial result = new Polynomial();
		for (Monomial m : this.listaMonoame) {
			result.insertMonomial(m);
		}

		for (Monomial m2 : p.listaMonoame) {
			boolean found = false;

			for (Monomial m : this.listaMonoame) {
				if (m.getExp() == m2.getExp()) {
					found = true;
					m.add(m2);
					break;
				}
			}
			if (!found) {
				result.insertMonomial(m2);
			}
		}
		return result;
	}

	public Polynomial substraction(Polynomial p) {

		Polynomial result = new Polynomial();
		Monomial aux = new Monomial(0, 0);
		for (Monomial m : this.listaMonoame) {
			result.insertMonomial(m);
		}

		for (Monomial m2 : p.listaMonoame) {
			boolean found = false;

			for (Monomial m : this.listaMonoame) {
				if (m.getExp() == m2.getExp()) {
					found = true;
					m.sub(m2);
					result.removeMonomial(m);
					break;
				}
			}
			if (!found) {
				aux = m2;
				aux.coeff = aux.getCoeff() * (-1);
				result.insertMonomial(aux);
			}
		}
		result.reduce();
		return result;
	}

	public Polynomial multiply(Polynomial p) {
		Polynomial result = new Polynomial();

		for (Monomial m : p.listaMonoame) {
			for (Monomial m1 : this.listaMonoame) {
				result.insertMonomial(m.mul(m1));
			}
		}
		result.reduce();
		return result;
	}

	public Polynomial divide(Polynomial p) {
		// divides "this" by p
		Polynomial result = new Polynomial(); // the final result
		Polynomial aux = new Polynomial(); // the residue
		Polynomial r = new Polynomial();
		// put the polynomials in the right form
		this.reduce();
		p.reduce();
		// get the first monomial (which by now has the greatest exponent) of
		// each polynomial
		Monomial m1 = new Monomial(0, 0);
		Monomial m2 = new Monomial(0, 0);
		m1 = this.getListaMonoame().get(0);
		m2 = p.getListaMonoame().get(0);
		Monomial res = new Monomial(0, 0);
		while (m1.getExp() >= m2.getExp()) {
			res = m1.div(m2);
			result.insertMonomial(res);
			r.insertMonomial(res);
			aux = r.multiply(p);
			aux = this.substraction(aux);
			m1 = aux.getListaMonoame().get(0);// get the new monomial with the
												// greatest exponent
			r.listaMonoame.remove(res);
		}
		result.reduce();
		return result;
	}

	public Polynomial derive() {

		Polynomial result = new Polynomial();
		for (Monomial m : this.listaMonoame) {
			if(m.exp>0)
			result.insertMonomial(m.deriv());
		}

		return result;
	}

	public Polynomial integrate() {

		Polynomial result = new Polynomial();
		for (Monomial m : this.listaMonoame) {
			result.insertMonomial(m.integ());

		}

		return result;
	}


	public ArrayList<Monomial> getListaMonoame() {
		return listaMonoame;
	}

	public Monomial getMonom(int index) {
		return listaMonoame.get(index);
	}
	
	public int size() {
		return listaMonoame.size();
	}

	public void setListaMonoame(ArrayList<Monomial> listaMonoame) {
		this.listaMonoame = listaMonoame;
	}

	public boolean isEmpty() {
		return listaMonoame.isEmpty();
	}

	public String toString() {
		if (isEmpty()) {
			return "0";
		} else {
			String polyStr = "";
			float c; // coeff
			int e; // exp
			for (Monomial m : listaMonoame) {
				c = m.getCoeff();
				e = m.getExp();
				if ((c > 0) && !polyStr.isEmpty()) {
					polyStr = polyStr + "+";
				}
				polyStr = polyStr + m.toString();
			}
			return polyStr;
		}
	}
	
	  private static final Pattern POLY_PATTERN = Pattern.compile("([-+]?[0-9\\.]+)(x?)(\\^?)(\\d+)?");
      public static Polynomial fromString(String fxPoly) throws NumberFormatException {
              Polynomial poly = new Polynomial();
              final Matcher polyMatcher = POLY_PATTERN.matcher(fxPoly);
              while (polyMatcher.find()) {
                      // first item is coeff
                      float coeff = Float.parseFloat(polyMatcher.group(1));
                      int exp = 0;
                      if (polyMatcher.group(2).equals("x")) {
                              if (polyMatcher.group(3).equals("^")) {
                                      exp = Integer.parseInt(polyMatcher.group(4));
                              } else {
                                      exp = 1;
                              }
                      }
                      poly.insertMonomial(new Monomial(coeff, exp));
              }                
              return poly;
      }

}
