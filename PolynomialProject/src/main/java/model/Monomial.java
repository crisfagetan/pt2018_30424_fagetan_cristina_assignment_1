package model;

public class Monomial implements Comparable {

	public float coeff;
	public int exp;

	public Monomial(float coeff, int exp) {
		this.coeff = coeff;
		this.exp = exp;
	}

	public float getCoeff() {
		return coeff;
	}

	public void setCoeff(float coeff) {
		this.coeff = coeff;
	}

	public int getExp() {
		return exp;
	}

	public void setExp(int exp) {
		this.exp = exp;
	}

	//we suppose they have the same exponent
	public Monomial add(Monomial m) {
			coeff = coeff + m.getCoeff();
		return this;
	}

	public Monomial sub(Monomial m) {
		coeff = coeff - m.getCoeff();
		return this;
	}

	public Monomial mul(Monomial m) {
		Monomial result = new Monomial(0, 0);
		result.coeff = this.coeff * m.getCoeff();
		result.exp = this.exp + m.getExp();
		return result;
	}

	public Monomial div(Monomial m) {
		Monomial result = new Monomial(0, 0);
		result.coeff = this.coeff / m.getCoeff();
		result.exp = this.exp - m.getExp();
		return result;
	}

	public Monomial deriv() {
		Monomial result = new Monomial(0,0);
		if(exp == 0)
			return result;
		coeff = coeff * exp;
		exp = exp - 1;
		return this;
	}

	public Monomial integ() {
		coeff = coeff / (exp + 1);
		exp = exp + 1;
		return this;
	}

	public int compareTo(Object m) {
		int compareExp = ((Monomial) m).getExp();
		// For Descending Order
		return compareExp - this.exp;
	}

	@Override
	public String toString() {
		String str = "";
		// if the value is integer add no dots 
		if (coeff - (int)coeff == 0) {
			str = str + (int) coeff; 
		} else {
			str = str + coeff; 
		}
		if (exp == 1) {
			str = str +  "x"; 
		} else if (exp > 1) {
			str = str +  "x^" + exp; 
		}
		return str;
	}


}
