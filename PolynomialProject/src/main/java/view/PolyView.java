package view;

import java.awt.Color;
import java.awt.event.ActionListener;
import javax.swing.*;

public class PolyView extends JFrame {

	private JLabel fxLabel = new JLabel("f(x) = ");
	private JTextField fxPoly = new JTextField(200);
	private JLabel gxLabel = new JLabel("g(x) = ");
	private JTextField gxPoly = new JTextField(200);

	private JButton addButton = new JButton("f + g");
	private JButton sub1Button = new JButton("f - g");
	private JButton sub2Button = new JButton("g - f");
	private JButton mulButton = new JButton("f * g");
	private JButton div1Button = new JButton("f / g");
	private JButton div2Button = new JButton("g / f");
	private JButton derivfButton = new JButton("f'");
	private JButton derivgButton = new JButton("g'");
	private JButton integfButton = new JButton("Integrate f");
	private JButton integgButton = new JButton("Integrate g");

	private JLabel hxLabel = new JLabel("h(x) = ");
	private JTextField hxPoly = new JTextField(200);

	public PolyView() {

		JPanel main = new JPanel();
		main.setLayout(null);
		this.add(main);

		hxPoly.setEditable(false);

		fxLabel.setBounds(10, 30, 50, 20);
		fxPoly.setBounds(70, 30, 350, 20);
		main.add(fxLabel);
		main.add(fxPoly);

		gxLabel.setBounds(10, 60, 50, 20);
		gxPoly.setBounds(70, 60, 350, 20);
		main.add(gxLabel);
		main.add(gxPoly);

		addButton.setBounds(10, 90, 60, 30);
		sub1Button.setBounds(80, 90, 60, 30);
		sub2Button.setBounds(150, 90, 60, 30);
		mulButton.setBounds(220, 90, 60, 30);
		div1Button.setBounds(290, 90, 60, 30);
		div2Button.setBounds(360, 90, 60, 30);
		main.add(addButton);
		main.add(sub1Button);
		main.add(sub2Button);
		main.add(mulButton);
		main.add(div1Button);
		main.add(div2Button);

		derivfButton.setBounds(40, 130, 60, 30);
		derivgButton.setBounds(110, 130, 60, 30);
		integfButton.setBounds(180, 130, 100, 30);
		integgButton.setBounds(290, 130, 100, 30);
		main.add(derivfButton);
		main.add(derivgButton);
		main.add(integfButton);
		main.add(integgButton);

		hxLabel.setBounds(10, 170, 50, 20);
		hxPoly.setBounds(70, 170, 350, 20);
		main.add(hxLabel);
		main.add(hxPoly);

		this.setTitle("Polynomial Calculator");
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setSize(450, 300);
		this.setLocationRelativeTo(null);
		this.setContentPane(main);
		main.setBackground(new Color(206, 177, 221));
	}


	public String getFxPoly() {
		return fxPoly.getText();
	}

	public String getGxPoly() {
		return gxPoly.getText();
	}

	public void setFxPoly(String fx) {
		fxPoly.setText(fx);
	}
	
	public void setGxPoly(String gx) {
		gxPoly.setText(gx);
	}
	
	public void setHxPoly(String hx) {
		hxPoly.setText(hx);
	}

	public void addListener(ActionListener listenButton) {
		addButton.addActionListener(listenButton);
	}
	
	public void sub1Listener(ActionListener listenButton) {
		sub1Button.addActionListener(listenButton);
	}
	
	public void sub2Listener(ActionListener listenButton) {
		sub2Button.addActionListener(listenButton);
	}
	
	public void mulListener(ActionListener listenButton) {
		mulButton.addActionListener(listenButton);
	}
	
	public void div1Listener(ActionListener listenButton) {
		div1Button.addActionListener(listenButton);
	}
	
	public void div2Listener(ActionListener listenButton) {
		div2Button.addActionListener(listenButton);
	}
	
	public void derivfListener(ActionListener listenButton) {
		derivfButton.addActionListener(listenButton);
	}
	
	public void derivgListener(ActionListener listenButton) {
		derivgButton.addActionListener(listenButton);
	}
	
	public void integfListener(ActionListener listenButton) {
		integfButton.addActionListener(listenButton);
	}
	
	public void integgListener(ActionListener listenButton) {
		integgButton.addActionListener(listenButton);
	}
	

	
}
