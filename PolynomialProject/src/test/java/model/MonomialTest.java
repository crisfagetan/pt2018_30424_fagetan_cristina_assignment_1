package model;

import junit.framework.TestCase;

public class MonomialTest extends TestCase {

	public void testAdd() {
		Monomial m = new Monomial(4,2);
		Monomial m1 = new Monomial(6,2);
		assertEquals("10x^2", m.add(m1).toString());
	}

	public void testSub() {
		Monomial m = new Monomial(4,2);
		Monomial m1 = new Monomial(6,2);
		assertEquals("-2x^2", m.sub(m1).toString());
		
		m = new Monomial(4,2);
		m1 = new Monomial(6,2);
		assertEquals("2x^2", m1.sub(m).toString());
	}

	public void testMul() {
		Monomial m = new Monomial(3,2);
		Monomial m1 = new Monomial(2,2);
		assertEquals("6x^4", m.mul(m1).toString());
		
		m = new Monomial(1,7);
		m1 = new Monomial(-6,3);
		assertEquals("-6x^10", m1.mul(m).toString());
	}

	public void testDiv() {
		Monomial m = new Monomial(4,4);
		Monomial m1 = new Monomial(2,3);
		assertEquals("2x", m.div(m1).toString());
		
		m = new Monomial(12,7);
		m1 = new Monomial(-6,3);
		assertEquals("-2x^4", m.div(m1).toString());
	}

	public void testDeriv() {
		Monomial m = new Monomial(4,0);
		assertEquals("0", m.deriv().toString());
		
		m = new Monomial(10,1);
		assertEquals("10", m.deriv().toString());
		
		m = new Monomial(5,2);
		assertEquals("10x", m.deriv().toString());
	}

	public void testInteg() {
		Monomial m = new Monomial(4,0);
		assertEquals("4x", m.integ().toString());
		
		m = new Monomial(10,1);
		assertEquals("5x^2", m.integ().toString());
		
		m = new Monomial(6,2);
		assertEquals("2x^3", m.integ().toString());
	}

	public void testToString() {
		Monomial m = new Monomial(4,0);
		assertEquals("4", m.toString());
		
		m = new Monomial(10,1);
		assertEquals("10x", m.toString());
		
		m = new Monomial(10.3f,3);
		assertEquals("10.3x^3", m.toString());
	}

}
