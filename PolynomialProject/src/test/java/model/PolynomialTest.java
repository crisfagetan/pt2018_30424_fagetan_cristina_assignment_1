package model;

import junit.framework.TestCase;

public class PolynomialTest extends TestCase {

	// test new created Poly is empty
	public void testIsEmpty() {
		Polynomial poly = new Polynomial();
		assertTrue(poly.isEmpty());
	}

	
	// test insertion
	public void testInsertMonomial() {
		Polynomial poly = new Polynomial();
		// insert new mono
		poly.insertMonomial(new Monomial(10, 2));
		// check insertion increased the poly size and the monoms is properly stored 
		assertEquals(1, poly.size());
		assertEquals("10x^2", poly.getMonom(0).toString());
	}

	// test single mono/poly string representation
	public void testToString() {
		Polynomial poly = new Polynomial();
		// insert monoms
		poly.insertMonomial(new Monomial(10, 2)); // exp > 1 
		poly.insertMonomial(new Monomial(-2, 1)); // negative coeff & exp = 1 
		poly.insertMonomial(new Monomial(3.2f, 0)); // float coef & exp = 0
		assertEquals("10x^2-2x+3.2", poly.toString());
	}
	
	public void testFromString() {
		Polynomial poly = Polynomial.fromString("10x^3+5.2x+22+1x");
		// expect 3 monoms
		assertEquals(4, poly.size());
		// check first monom
		assertEquals("10x^3", poly.getMonom(0).toString());
		// check second monom (exp = 1)
		assertEquals("5.2x", poly.getMonom(1).toString());
		// check second monom (exp = 0)
		assertEquals("22", poly.getMonom(2).toString());
		assertEquals("1x", poly.getMonom(3).toString());
	}

	
	 public void testReduce() {
		 Polynomial poly = Polynomial.fromString("10x^3+5x^3+22x+3+5");
		 poly.reduce();
		 assertEquals("15x^3+22x+8", poly.toString());
	 }
	
	 public void testAddition() {
		 Polynomial fx = Polynomial.fromString("1x^2+3x+1");
		 Polynomial gx = Polynomial.fromString("1x+5");
		 assertEquals("1x^2+4x+6", fx.addition(gx).toString());
	 }
	
	 public void testSubstraction() {
		 Polynomial fx = Polynomial.fromString("1x^2+3x+1");
		 Polynomial gx = Polynomial.fromString("1x+5");
		 assertEquals("1x^2+2x-4", fx.substraction(gx).toString());
		 fx = Polynomial.fromString("1x^2+3x+1");
		 assertEquals("-1x^2-2x+4", gx.substraction(fx).toString());
	 }
	
	 public void testMultiply() {
		 Polynomial fx = Polynomial.fromString("1x^2+3x+1");
		 Polynomial gx = Polynomial.fromString("1x+5");
		 assertEquals("1x^3+8x^2+16x+5", fx.multiply(gx).toString());
	 }
	
	 public void testDivide() {
		 Polynomial fx = Polynomial.fromString("1x^2+3x+1");
		 Polynomial gx = Polynomial.fromString("1x+5");
		 assertEquals("1x-2", fx.divide(gx).toString());
	 }
	
	 public void testDerive() {
		 Polynomial fx = Polynomial.fromString("10x^3+5x^1+22x^0");
		 assertEquals("30x^2+5", fx.derive().toString());
	 }
	
	 public void testIntegrate() {
		 Polynomial fx = Polynomial.fromString("3x^2+4x+1");
		 assertEquals("1x^3+2x^2+1x", fx.integrate().toString());
	 }
	
}
